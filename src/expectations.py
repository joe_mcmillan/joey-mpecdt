import numpy as np
import pylab

"""
File containing code relating to Computational Lab 1 of the Dynamics
MPECDT core course.

You should fork this repository and add any extra functions and
classes to this module.
"""

def quadrature(f, b, c):
    """
    Function to return the quadrature approximation of 
    \int f(x) \pi(x) dx
    for some PDF \pi(x), with weights b and quadrature points c.
    
    Inputs:
    f - the function to integrate, which takes x as input and returns f(x) 
        as output
    b - numpy array of the quadrature weights
    c - numpy array of the quadrature points

    Outputs:
    I - the value of the integral
    """

    I = 0.
    for i,b_i in enumerate(b):
        I += f(c[i])*b_i
    return I

def Unit(x):
    return 1.0

GaussLegendre1_b = [0.5,0.5]
GaussLegendre1_c = [0.5*(1-3.0**(-0.5)),0.5*(1+3.0**(-0.5))]

if __name__ == '__main__':    
    print "GL Integral is "+str(quadrature(Unit,GaussLegendre1_b,
                                        GaussLegendre1_c))
                                        


def quadrature(f, b, c):
    """
    Function to return the quadrature approximation of 
    \int f(x) \pi(x) dx
    for some PDF \pi(x), with weights b and quadrature points c.
    
    Inputs:
    f - the function to integrate, which takes x as input and returns f(x) 
        as output
    b - numpy array of the quadrature weights
    c - numpy array of the quadrature points

    Outputs:
    I - the value of the integral
    """

    I = 0.
    for i,b_i in enumerate(b):
        I += f(c[i])*b_i
    return I

def f(x):
    return -3*x**3+2*x-4

GaussHermite1_b = [0.5,0.5]
GaussHermite1_c = [-1,1]

if __name__ == '__main__':    
    print "GH Integral is "+str(quadrature(f,GaussHermite1_b,
                                        GaussHermite1_c))


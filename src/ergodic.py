import numpy as np
import pylab
import matplotlib.pyplot as pl
import matplotlib.animation as animation
import time
from mpl_toolkits.mplot3d import Axes3D
from scipy import integrate
from data import getdata
from data import plot3D

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def ensemble_plot(Ens,title=''):
    """
    Function to plot the locations of an ensemble of points.
    """

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Ens[:,0],Ens[:,1],Ens[:,2],'.')    
    pylab.title(title)

def ensemble(M,T):
    """
    Function to integrate an ensemble of trajectories of the Lorenz system.
    Inputs:
    M - the number of ensemble members
    T - the time to integrate for
    """
    ens = 0.1*np.random.randn(M,3)

    for m in range(M):
        t = np.array([0.,T])
        data = integrate.odeint(vectorfield, 
                                y0=ens[m,:], t=t, args=(10.0,28.,8./3.),
                                mxstep=100000)

        ens[m,:] = data[1,:]

    return ens

def f(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f: - 1d- array of f values
    """

    return d[:,0]**2 + d[:,1]**2 + d[:,2]**2

def ex1(M,T):
    
# Creates our ensemble plot for choosen M and T 
    
    Ens = ensemble(M, T)
    ensemble_plot(Ens,'T={time}'.format(time = T))
    pylab.show()

def spaceAVG(M = [1, 5, 10, 20], T = [5, 10, 20], N = 50, f = f):
    
    avg = np.zeros(N, dtype=float)
    
    for t in T:
        
        print ' '        
        print ' '
        print 'Time: ',t,':'
        print ' '
        
        for m in M:
            
            for i in range(0,N):   
                avg[i] = np.average(f(ensemble(m,t)))
                
            print ' '               
            print '     ** M = ',m,' **'
            print 'Variance of averages :', np.var(avg)
 
def timeAVG(dT = 0.1, f = f):
    
    for j in range(3):   
    
        N = np.arange(50, 1500, 50)
        avg = np.zeros_like(N, dtype=float)
        
        for i in range(len(N)):
            
            t = np.arange(0.,N[i]*dT, dT)
            soln = integrate.odeint(vectorfield, np.random.randn(3), t=t, args=(10.,28.,8./3.))
            avg[i] = np.average(f(soln))
            
        pl.plot(N, avg, label = 'plot: '+str(j+1))
    pl.legend(loc='best')
    pl.title('Time Average Plot w/ Random IC')
    pl.xlabel('$N$')
    pl.ylabel('Time Average')
    pl.show()

def getData(IC = np.random.randn(3) , dT=0.01, args=(10., 28., 8./3)):

    t = np.arange(10000, 10200, dT)
    data = integrate.odeint(vectorfield, IC, t=t, args=args)
    
    return data, t

def sigma(sigma = [40., 5., 10., 20.], rho = 28., beta = 8./3):
    
    pl.title(r'Varying $\sigma$', fontsize=(24))    
    pl.axis('off')    
    
    for i in range(4):
        
        args = (sigma[i], rho, beta)
        data, time = getData(args = args)
        color = ['b', 'g', 'r', 'c']
                      
        fig = pl.figure(1)
        ax = fig.add_subplot(2,2,i, projection='3d')
        ax.plot(data[:,0],data[:,1],data[:,2], color = color[i])
        
        pl.title(r'$(\sigma, \rho, \beta) =$ ('"*%.f*, %.f, %.2f" % (args)+')')
        pl.xlabel('$x$')
        pl.ylabel('$y$')
        
    pl.show()
    
def rho(sigma = 10., rho = [118., 14., 28., 56.], beta = 8./3):
    
    pl.title(r'Varying $\rho$', fontsize=(24))    
    pl.axis('off') 
        
    for i in range(4):
        
        args = (sigma, rho[i], beta)
        data, time = getData(args = args)
        color = ['b', 'g', 'r', 'c']
                      
        fig = pl.figure(1)
        ax = fig.add_subplot(2,2,i, projection='3d')
        ax.plot(data[:,0],data[:,1],data[:,2], color = color[i])     
        pl.title(r'$(\sigma, \rho, \beta) =$ ('"%.f, *%.f*, %.2f" % (args)+')')
        pl.xlabel('$x$')
        pl.ylabel('$y$')

    pl.show()
       
def beta(sigma = 10., rho = 28., beta = [32./3, 4./3, 8./3., 16./3]):
    
    pl.title(r'Varying $\beta$', fontsize=(24)) 
    pl.axis('off')    
    
    for i in range(4):
        
        args = (sigma, rho, beta[i])
        data, time = getData(args = args)
        color = ['b', 'g', 'r', 'c']
                      
        fig = pl.figure(1)
        ax = fig.add_subplot(2,2,i, projection='3d')
        ax.plot(data[:,0],data[:,1],data[:,2], color = color[i])

        
        pl.title(r'$(\sigma, \rho, \beta) =$ ('"%.f, %.f, *%.2f*" % (args)+')')
        pl.xlabel('$x$')
        pl.ylabel('$y$')

    pl.show()

    

#Excersise 1:
 
#~ for T in [0.01, 0.5, 1, 10, 100]:
    #~ ex1(1000,T)



#Ecersise 2

print '***   Excersise 2   ***'
print ''
print '  ***   Part 1   ***'
print ''
print ''
print 'First we compute the spatial average, varying time (T) and #points (M).'
print 'We will also look at the variance of our average for each of these M,T.'
print ''
print ''

spaceAVG()

print ''
print ''
print 'We notice that:'
print 'As T increases the variance increases.'
print 'As M increases the the varience decreases.'
print ''
print ''

time.sleep(4)



print '***   Excersise 2   ***'
print ''
print '  ***   Part 2   ***'
print ''
print ''
print 'We now take a look at how our time average aproximation changes for '
print 'a fixed function, f, fixed \Delta f and increasing N.'

time.sleep(4)

print ''
print 'We plot creat multiple plots each with different random initial conditions.'

time.sleep(3)

timeAVG()

print ''
print ''
print 'We notice that despite the innitial conditions being random (and different).'
print 'we get convergence (at an approximatetely exponential rate (exp(-0.007)).'

time.sleep(4)


#Excersise 3

print ''
print ''
print '***   Excersise 3   ***'
print ''
print '  ***   Part 1   ***'
print ''
print ''
print 'We look at what happens to the attractor shape for large time, T.'
print 'We experiment by varying different parametres to see what happens.'
print ''
print ''

time.sleep(5)

print 'First we will vary sigma.'

time.sleep(1)

sigma()

print ''
print ''
print 'We notice that when we increased and decreased sigma we loose the stable'
print 'double torus shape and instead create a fixed point.'
print ''
print ''

time.sleep(5)

print 'Next we vary rho.'

time.sleep(1)

rho()

print ''
print ''
print 'This time we only create a fixed point when we decrease rho'
print 'for the other plots the solution is mostly unchanged.'
print ''
print ''

time.sleep(5)

print 'Finally we vary beta.'

time.sleep(1)

beta()

print ''
print ''
print 'For beta we can the fixed point instead when we increase.'
print 'We see this is the final 2 plots.'

time.sleep(5)

print ''
print ''
print 'Overall varying any of the parametres can have an effect on the'
print 'shape of the attractor. But sigma seems to have the largest effect.'

time.sleep(5)

print ''
print ''
print '***   Excersise 3   ***'
print ''
print '  ***   Part 2   ***'
print ''
print ''

time.sleep(1)

print '5!'
print ''

time.sleep(1)

print '4!'
print ''

time.sleep(1)

print '3!'
print ''

time.sleep(1)

print '2!'
print ''

time.sleep(1)

print '1!'
print ''

time.sleep(1)

print ''
print 'Unfortunately I didnt do this part - sorry for the anticlimax.'

time.sleep(3)

print ''
print ''
print '   **********   THE END   **********   '
print ''
print ''

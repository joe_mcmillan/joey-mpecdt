import numpy as np
import pylab as pl

def lmap(x):
    
   """
   Function to evaluate y = a*x + b for a = 2, b = 1

   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array containing the double well polynomial evaluated on x
   """
   
   a = 2
   b = 1

   return a*x + b

def idi(x):
    
   """
   Identity function

   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array of values
   """

   return x

def myprior():
    
   """
   Function to draw a sample from a prior distribution given by the
   normal distribution with variance alpha.
   inputs: none
   outputs: the sample
   """
   
   alpha = 1.0
   return alpha**0.5*np.random.randn()

def Phi(f,x,y):

   """
   Function to return Phi, assuming a normal distribution for 
   the observation noise, with mean 0 and variance sigma.
   inputs:
   f - the function implementing the forward model
   x - a value of the model state x
   y - a value of the observed data
   """

   sigma = 1.0e-1
   return (f(x)-y)**2/sigma**2

def mcmc(f, prior_sampler, x0, yhat, N, beta):

   """
   Function to implement the MCMC pCN algorithm.
   inputs:
   f - the function implementing the forward model
   prior_sampler - a function that generates samples from the prior
   x0 - the initial condition for the Markov chain
   yhat - the observed data
   N - the number of samples in the chain
   beta - the pCN parameter

   outputs:
   xvals - the array of sample values
   avals - the array of acceptance probabilities
   """

   xvals = np.zeros(N+1)
   xvals[0] = x0
   avals = np.zeros(N)

   for i in range(N):

      v = (1.-beta**2.)**(1./2.)*xvals[i]+beta*prior_sampler()
      avals[i] = np.minimum(1,np.exp(Phi(f, xvals[i], yhat)-Phi(f, v, yhat)))
      u = np.random.uniform(0,1)      
      if u<avals[i]:
         xvals[i+1] = v
      else:
         xvals[i+1] = xvals[i]
   return xvals,avals

def betacalc(prob, accuracy):

    """
    Function to implement find beta to a given accuracy and propability

    inputs:
    prob - probability of acceptance, number between 0 and 1 (normally 0.25)
    accuracy - some small number c. 0.001

    outputs:
    required beta value
    """  

    a = 0.
    b = 1.
    beta = (a+b)/2.
    xvals,avals = mcmc(lmap,myprior,x0,yhat,N,beta)

    while np.abs((np.cumsum(avals)/np.arange(1,len(np.cumsum(avals))+1))[-1]-prob)>accuracy:
        if (np.cumsum(avals)/np.arange(1,len(np.cumsum(avals))+1))[-1]>prob:
            a = beta
        else:
           b = beta
        beta = (a+b)/2.
        xvals,avals = mcmc(lmap,myprior,x0,yhat,N,beta)

    print "Beta equals %f" %(beta)

    return beta


if __name__ == '__main__':   

    yhat = 1.
    x0 = 0.

    N = 1000
    b = betacalc(0.25, 0.0001)
 
    xvals,avals = mcmc(idi,myprior,x0,yhat,N,b)
    average = np.cumsum(avals)/np.arange(1,avals.size+1)

    pl.plot(np.arange(1,avals.size+1),average)
    pl.show()

    print "a average: %f" %(average[999])

    ab = xvals[300:]
    mean = np.mean(ab)
    var = np.var(ab)

    pl.hist(ab, normed=1)
    pl.show()

    print "x mean: %f, x var: %f" %(mean, var)

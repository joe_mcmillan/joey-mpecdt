import numpy as np
import pylab
import matplotlib.pyplot as pl

"""
File containing code relating to Computational Lab 3 of the Dynamics
MPECDT core course.
"""

def square(x):
    """
    Function to compute the square of the input. If the input is 
    a numpy array, this returns a numpy array of all of the squared values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the square of all of the values
    """
    return x**2

def normal(N):
    """
    Function to return a numpy array containing N samples from 
    a N(0,1) distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.randn(N)

def normal_pdf(x):
    """
    Function to evaluate the PDF for the normal distribution (the
    normalisation coefficient is ignored). If the input is a numpy
    array, this returns a numpy array with the PDF evaluated at each
    of the values in the array.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    return np.exp(-x**2/2)

def uniform_pdf(x):
    """
    Function to evaluate the PDF for the standard uniform
    distribution. If the input is a numpy array, this returns a numpy
    array of all of the squared values.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    y = 0*x
    y[x>0] = 1.0
    y[x>1] = 0.0
    return y

def importance(N, Y, f, rho, rhoprime):
    """
    Function to compute the importance sampling estimate of the
    expectation E[f(X)], with N samples

    inputs:
    f - a Python function that evaluates a chosen mathematical function on
    each entry in a numpy array
    Y - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    np.zeros_like(x)    probability distribution
    rho - a Python function that evaluates the PDF for the desired distribution
    on each entry in a numpy array
    rhoprime - a Python function that evaluates the PDF for the
    distribution for Y, on each entry in a numpy array
    N - the number of samples to use
    """
    
    
    theta = (1/(np.sum(rho/rhoprime)))*np.sum(np.dot(f,rho/rhoprime))

    return theta
    
def cluster(N):
    """
    Function to return a numpy array containing N samples from 
    a clustered distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    
    U = np.random.uniform(0,1,N)
    X = -0.1*np.log(1-U+U*np.exp(-10))
    return X

def cluster_pdf(x):
    """
    Function to evaluate the PDF for the clustered distribution (above). 
    If the input is a numpy array, this returns a numpy array with the 
    PDF evaluated at each of the values in the array.

    inputs:
    x - a numpy array of input values
    
    outputs:
    y - a numpy array of y(x)
    """

    y = 10*np.exp(-10*x)/(1-np.exp(-10))

    return y
    
def function(x):
   """
   Function used in question 2. If the input is 
   a numpy array, this returns a numpy array of all of the squared values.
   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array containing the square of all of the values
   """
   return np.exp(-10*x)*np.cos(x)

def uniform(N):
    """
    Function to return a numpy array containing N samples from 
    a U(0,1) distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.uniform(0,1,N)
       
if __name__ == '__main__':
    
    print '***   Excersise 1   ***'
    print ''
    print '  ***   Part 1   ***'
    print ''
    print ''
    print 'Firt we complete the code for the importance method and calculate a'
    print 'value for theta for fixed N = 100. In the example we predict for the'
    print 'uniform distribution using what we know about the normal distribtion.'
    print 'We take f=x^2.'
    print ''
    print ''
    
    N = 100
    Y = normal(N) 
    f = square(Y)
    rho = uniform_pdf(Y)
    rhoprime = normal_pdf(Y)
    
    theta = importance(N, Y , f, rho, rhoprime)
    
    print 'theta =', theta


    print ''
    print ''
    print '***   Excersise 1   ***'
    print ''
    print '  ***   Part 2   ***'
    print ''
    print ''
    print 'Using the fact that we know the expected value in the case is 1/3'
    print 'we can plot the error of our importance method for increasing N.'
    print ''
    print ''


    print '* PLOT *'
    
    
    N = np.power(10, np.arange(1., 5., 0.02))
    theta = np.zeros_like(N)

    for n, i in zip(N, range(np.size(N))):
        Y = normal(n)
        f = square(Y)
        rho = uniform_pdf(Y)
        rhoprime = normal_pdf(Y)
        theta[i] = ((importance(n, Y, f, rho, rhoprime)))
        
    error = np.abs(theta - 1./3.)
    
    pl.loglog(N, error, label = 'importance error')
    pl.title('Importance Error against N')
    pl.xlabel('$N$')
    pl.ylabel('Error')
    pl.show()

    print '* PLOT *'


    print ''
    print ''
    print 'We see that this converges to zero (exponentially)'
    print ''
    print ''


    print ''
    print ''
    print '***   Excersise 2   ***'
    print ''
    print '  ***   Part 1   ***'
    print ''
    print ''

    print 'We now look at using the importance sampling method to '
    print 'calculate the expectation for a different function and distribution.'
    print ''
    print ''
    print ''


    print '* PLOT *'

    N = np.power(10, np.arange(1., 5., 0.02))
    theta = np.zeros_like(N)
    montecarlo = np.zeros_like(N)
    
    for n, i in zip(N, range(np.size(N))):
        
        Y = cluster(n)
        Y2 = uniform(n)   # for use in MC method
        f = function(Y)
        f2 = function(Y2)   # for use in MC method
        rho = uniform_pdf(Y)
        rhoprime = cluster_pdf(Y)
        
        theta[i] = importance(n, Y, f, rho, rhoprime)
        montecarlo[i] = np.sum(f2)/n   # for comparison
        
    exact = 10./101.-(10.*np.cos(1.)-np.sin(1.))/(101.*np.exp(10.)) #from problem sheet
    error = np.abs(theta - exact)
    mcerror = np.abs(montecarlo - exact)    # for comparison

    pl.loglog(N, error, label = 'importance error')
    pl.loglog(N, mcerror, label = 'montecarlo error')   # for comparison
    pl.title('Importance Error against N')
    pl.legend(loc='best')
    pl.xlabel('$N$')
    pl.ylabel('Error')
    pl.show()

    print '* PLOT *'

    print ''
    print ''
    print 'We see from this plot that we get convergence to zero in the error'
    print '(exponentially) justifying the use of this method.'
    print ''
    print ''
    print 'In this plot we compare to using the regular montecarlo sampling method'
    print 'and notice that we haven\'t gained anything. This is fixed if we remove'
    print 'the normalising constant (re conversation with Colin).'
    print ''

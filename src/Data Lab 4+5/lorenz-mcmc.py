import numpy as np
import pylab as pl
from scipy import integrate


def myprior():
    
    """
    Function to draw a sample from a prior distribution given by the
    normal distribution with variance 0.5.
    
    inputs: none
    outputs: the sample
    """

    return 0.5*np.random.randn(3) + [-0.5, -0.55, 17]

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)

def Phi(f,x,y, sigma):
    """
    Function to return Phi, assuming a normal distribution for 
    the observation noise, with mean 0 and variance sigma.
    inputs:
    f - the function implementing the forward model
    x - a value of the model state x
    y - a value of the observed data
    sigma - standard deviation of data
    """
    fx = f(x,1,0.1)[0][-1,:]
    return np.linalg.norm(fx-y)**2/(2*sigma**2)

def getdata(y0,T,Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(10.0,28.,8./3.))
    return data, t
    
def mcmc(f, prior_sampler, x0, yhat, N, beta):

   xvals = np.zeros([N+1,3])
   xvals[0,:] = x0
   avals = np.zeros(N)

   for i in range(N):

      v = (1.-beta**2.)**(1./2.)*xvals[i,:]+beta*prior_sampler()
      avals[i] = np.minimum(1,np.exp(Phi(f, xvals[i,:], yhat, 0.5)-Phi(f, v, yhat, 0.5)))
      u = np.random.uniform(0,1)      
      if u<avals[i]:
         xvals[i+1,:] = v
      else:
         xvals[i+1,:] = xvals[i,:]
   return xvals,avals

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    args - an object containing parameters

    Outputs:
    dxdt - the value of dx/dt
    """

    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

if __name__ == '__main__': 
    
    x0 = [-0.587, -0.563, 16.87]
    data = getdata(x0, 1, 0.01)[0]

    print ""
    print "**********"
    print ""
    print ""
    print "Plot of our system at T=1"
    print ""
    print ""
    print "**********"
    print ""
 
    plot3D(data[:,0], data[:,1], data[:,2])
    pl.show()
    
    print ""
    print "**********"
    print ""
    print ""
    print "First we will find an appropriate beta value"
    print ""
    print ""
    print "**********"
    print ""
    
    print ""
    print "**********"
    print ""
    print ""
    print "Appologise for the approx 06:00 wait there is now :s"
    print ""
    print ""
    print "**********"
    print ""    
    
    N = 20000     
    yhat = data[-1,:] + np.random.randn(3)
    beta = [0.0004, 0.0001, 0.0002, 0.00025, 0.0003, 0.00035]
    beta1 = 0.00025 
    xvals,avals = mcmc(getdata, myprior, x0, yhat, N, beta1)  
      
    for i in range(6):
        
        xvals,avals = mcmc(getdata, myprior, x0, yhat, N, beta[i])
        average_a = np.cumsum(avals)/np.arange(1,avals.size+1)
        
        pl.figure(1)  
        pl.subplot(3,2,i)
        pl.plot(np.arange(1,avals.size+1),average_a)
        pl.axhline(y=0.25,color='r',ls='dashed')
        pl.title("beta=%f" %(beta[i]))
    pl.suptitle('Average rate of acceptance, varying beta', size=20)
    pl.show()
    
    print ""    
    print "**********"
    print ""
    print ""
    print "Now an appropriate burn in value, beta = 0.00025"
    print ""
    print ""
    print "**********"
    print ""
       
    pl.figure(1)
    pl.subplot(311)
    pl.plot(xvals[:,0], label = "X")
    pl.axvline(x=6000.,color='r',ls='dashed', label = "Burn in point")
    pl.legend(loc='best')    
    pl.subplot(312)
    pl.plot(xvals[:,1], label = "Y")
    pl.axvline(x=6000.,color='r',ls='dashed', label = "Burn in point")
    pl.legend(loc='best')
    pl.subplot(313)
    pl.plot(xvals[:,2], label = "Z")
    pl.axvline(x=6000.,color='r',ls='dashed', label = "Burn in point")
    pl.legend(loc='best')    
    pl.suptitle('Deciding Burn In Point', size=20)
    pl.show()
    
    print ""
    print "**********"
    print ""
    print ""
    print "We now will look at the mean and variance after our burn in,"
    print "as well as the histogram given by each component after this point."
    print ""
    print ""
    print "**********"
    print ""
    
    abValue = 6000
    abX = xvals[abValue:,:]
    meanX = pl.mean(abX, axis=0)
    varX = np.var(abX, axis=0)
    
    print ""    
    print "**********"
    print ""
    print ""
    print "Var: ", varX
    print "Mean: ", meanX
    print ""
    print ""
    print "**********"
    print ""    
    
    pl.figure(1)
    pl.subplot(131)
    pl.hist(abX[:,0], normed=1, bins = 50)
    pl.title("Distribution of X")
    
    pl.subplot(132)
    pl.hist(abX[:,1], normed=1, bins = 50)
    pl.title("Distribution of Y")

    pl.subplot(133)
    pl.hist(abX[:,2], normed=1, bins = 50)
    pl.title("Distribution of Z")
    
    pl.show()
    
    print ""
    print "**********"
    print ""
    print ""
    print "some insightful conclusion"
    print ""
    print ""
    print "**********"
    print ""

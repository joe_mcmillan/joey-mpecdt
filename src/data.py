from scipy import integrate
import numpy as np
import pylab

"""
File containing code relating to Computational Lab 1 of the Dynamics
MPECDT core course.

This version contains partial solutions.
"""

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    args - an object containing parameters

    Outputs:
    dxdt - the value of dx/dt
    """

    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def getdata(y0,T,Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(10.0,28.,8./3.))
    return data, t

def linear_forecasting_onestep(x,time):
    """
    Function to make linear predictions from a time series of observations.
    Input:
    x - one dimensional array containing time series of x components from Lorenz solution.
    time - series of time values when the observations were taken.
    """
    
    #This makes a copy
    xp = 1.0*x
    for n in range(2,x.size):
        xp[n] = 2*x[n-1] - x[n-2]

    #Error only sums over forecast values so indexing from 2
    RMSerror = np.average((xp[2:]-x[2:])**2)**0.5
    print "RMS error for linear forecasting is:", RMSerror

    pylab.figure()
    pylab.plot(time[2:],xp[2:],'.',
               time[2:],x[2:],'-')
    pylab.legend(('Forecast','Observation'))
    pylab.xlabel('Time')
    pylab.ylabel('x component')
    pylab.title('Linear forecast one step ahead')

def linear_forecasting_nstep(x,time,step):
    """
    Function to make linear predictions from a time series of observations.
    Input:
    x - one dimensional array containing time series of x components from Lorenz solution.
    time - series of time values when the observations were taken.
    step - number of steps ahead to make the forecast.
    """
    
    #This makes a copy
    xp = 1.0*x
    for n in range(step+1,x.size):
        xp[n] = x[n-2] + step*(x[n-2] - x[n-3])

    #Error only sums over forecast values so indexing from 2
    RMSerror = np.average((xp[step+1:]-x[step+1:])**2)**0.5
    print "RMS error for linear forecasting is:", RMSerror

    pylab.figure()
    pylab.plot(time[step+1:],xp[step+1:],'.',
               time[step+1:],x[step+1:],'-')
    pylab.legend(('Forecast','Observation'))
    pylab.xlabel('Time')
    pylab.ylabel('x component')
    pylab.title('Linear forecast '+str(step)+' steps ahead')

def poly_forecasting_nstep(x,time,p,step):
    """
    Function to make linear predictions from a time series of observations.
    Input:
    x - one dimensional array containing time series of x components from Lorenz solution.
    time - series of time values when the observations were taken.
    p - polynomial degree to use in the forecasts
    step - number of steps ahead to make the forecast.
    """
    
    #This makes a copy
    xp = 1.0*x
    for n in range(step+p,x.size):
        t = np.arange(-p,1)
        y = x[n-(p+step):n-step+1]
        #get array of coefficients for fitting polynomial
        z = np.polyfit(t,y,p)
        #construct poly1d object for easy evaluation
        pol = np.poly1d(z)
        xp[n] = pol(step)

    #Error only sums over forecast values so indexing from 2
    RMSerror = np.average((xp[step+p:]-x[step+p:])**2)**0.5
    print "RMS error for polynomial forecasting is:", RMSerror

    pylab.figure()
    pylab.plot(time[step+p:],xp[step+p:],'.',
               time[step+p:],x[step+p:],'-')
    pylab.legend(('Forecast','Observation'))
    pylab.xlabel('Time')
    pylab.ylabel('x component')
    if step<2:
        r = ''
    else:
        r = 's'

    pylab.title(str(p)+' degree polynomial forecast '+str(step)+' step'+r+' ahead')

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)

if __name__ == '__main__':

    #Exercise 1
    mydat, mytime = getdata([-0.587,-0.563,16.870],100.0,0.05)    
    plot3D(mydat[:,0],mydat[:,1],mydat[:,2])

    #Exercise 2
    linear_forecasting_onestep(mydat[:,0],mytime)

    #Exercise 3
    linear_forecasting_nstep(mydat[:,0],mytime,step=2)

    #Exercise 4
    poly_forecasting_nstep(mydat[:,0],mytime,p=2,step=1)
    poly_forecasting_nstep(mydat[:,0],mytime,p=2,step=2)

    pylab.show()

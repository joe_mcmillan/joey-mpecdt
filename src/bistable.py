import pylab as pl
import numpy as np

def V_bistable(x):
    
    """
    A simple function
    """
        
    return (x**4)/4-(x**2)/2

def Vprime_bistable(x):
    
    """
    A simple function
    """
        
    return x**3-x

def f(x):
    
    """
    A simple function
    """
    
    return x

def f2(x):
    
    """
    A simple function
    """
        
    return x**2

def brownian_dynamics(x, Vp, sigma, dt, T, tdump):
    
    """
    Solve the Brownian dynamics equation

    dx = -V(x)dt + sigma*dW

    using the Euler-Maruyama method

    x^{n+1} = x^n - V(x^n)dt + sigma*dW

    inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    """

    xvals = [x]
    t = 0.
    tvals = [0]
    dumpt = 0.
    
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += -Vp(x)*dt + sigma*dW
        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
            
    return np.array(xvals), np.array(tvals)

def pdf(x,sigma):
    
    """
    This function takes an array and returns an array as its first argument. 
    sigma - float value
    """
    
    PDF = np.zeros(np.size(x))
    
    for i in range(len(x)):
        
        PDF[i] = 0.0959202444556226945554*np.exp(-2./(sigma**2)*V_bistable(x[i])) # 0.0959202444556226945554 found using maple
        
    return PDF
  
def ensemble(N, initialx, sigma, dt, T, tdump):
    
    """
    inputs:
    N - size of the ensemble
    initialx - initial condition for x
    Vp - a function that returns V'(x) given x
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x
    x is initial condition for 
    """
    
    ensemble = np.zeros(N)
    
    for i in range(len(ensemble)):
        
        ensembleX, ensembleT = brownian_dynamics(initialx, Vprime_bistable, sigma, dt, T, tdump)
        ensemble[i] = ensembleX[-1]
        
    return ensemble
   
def average(function,sigma,dt,T,tdump):
    
    """
    inputs:
    f - function
    sigma - constant
    dt - the time step size
    T - the limit limit for the simulation
    tdump - the time interval between iterations
    
    outputs:
    average - the monte carlo approximation
    """
    
    x, t = brownian_dynamics(0.,Vprime_bistable,sigma,dt,T,tdump)
    fx = function(x)
    average = np.cumsum(fx)/np.arange(1,len(fx)+1)

    return average



# QUESTION 2

if __name__ == '__main__':
    
    print ''
    print ''
    print '***   Excersise 2   ***'
    print ''
    print ''
    
    T = [200, 10, 20, 50]
    
    for i,T in enumerate(T):
        
        N =750.
        initialx = 1.
        sigma = 0.5
        dt = 0.1
        T = T
        tdump = 0.
        
        ens = ensemble(N, initialx, sigma, dt, T, tdump)
        
        x = np.arange(-2,2,0.01)
        PDF = pdf(x, sigma)
        colors = ['b', 'g', 'r', 'c']
        
        pl.figure(1)
        pl.subplot(2,2,i)
        pl.hist(ens,bins = 80, normed = 1, label = 'Histogram', color = colors[i])
        pl.plot(x,PDF, linewidth = 2, label = 'PDF', color = 'black')
        pl.title('N =' +str(N)+'  $\sigma$ =' +str(sigma)+'  T =' +str(T) )
        pl.xlabel('x')
        pl.ylabel('Probability Density')
        pl.legend(loc='best')

    pl.show()

    print ''
    print ''
    print 'Clearly for the lower time our output is weighted to the right peak'
    print 'due to the fact that our initial condition was x = 1.'
    print ''
    print ''
      
    print ''
    print ''
    print 'We see that the convergence to the expected distribution (shown in black)'
    print 'is very fast - with little difference between T = 50 and T = 200.'
    print ''
    print ''
    
# QUESTION 3

if __name__ == '__main__':
    
    print ''
    print ''
    print '***   Excersise 3   ***'
    print ''
    print ''

    print ''
    print ''
    print 'We plot an approximation for the expectation of f(X) with f(x) = x and '
    print 'f(x) = x^2 and consider the rate of convergence using this method.'
    print ''
    print ''
  
    sigma = 0.5
    dt = 0.1
    T = 10000
    tdump = 0.1

    print ''
    print ''
    print 'First f(x) = x'
    print ''
    print ''

    avID = average(f,sigma,dt,T,tdump)
    errorID = np.abs(avID - 0)  # expected value is 0.

    pl.figure(1)

    pl.subplot(3,1,1)
    pl.plot(avID, label = 'Approx with $f(x)=x$')
    pl.xlim([0., 10000])
    pl.yticks([-1.0, -0.5, 0., 0.5, 1.0])
    pl.title('plot of values' )
    pl.legend(loc ='best')
    
    pl.subplot(3,1,2)
    pl.plot(errorID, color = 'r', label = 'Error with $f(x)=x$')
    pl.xlim([0., 10000])
    pl.yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
    pl.ylabel('$E[f(x)]$')    
    pl.title('Error plot' )
    pl.legend(loc ='best')
    
    pl.subplot(3,1,3)
    pl.semilogy(errorID, color = 'g', label = 'semilog plot of Error with $f(x)=x$')
    pl.plot(1/np.arange(1,100001)**0.25, label = '$1/N^{0.25}$', color = 'm')
    pl.yticks([0.000001, 0.0001, 0.01, 1.0])
    pl.title('loglog plot of error' )
    pl.xlabel('$N$')
    pl.legend(loc ='best')
    
    pl.show()

    print ''
    print ''
    print 'We see that the convergence looks exponential but with some'
    print 'computational/rounding errors for larger N.'
    print ''
    print ''

    print ''
    print ''
    print 'Now f(x) = x^2'
    print ''
    print ''

    avSQ = average(f2,sigma,dt,T,tdump)
    errorSQ = np.abs(avSQ - 0.85213615217833425903)  # expected value is 0.85213615217833425903 found using maple
    
    pl.figure(1)
    
    pl.subplot(3,1,1)
    pl.plot(avSQ, label = 'Approx with $f(x)=x^2$')
    pl.xlim([0., 1000])
    pl.yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
    pl.title('plot of values' )
    pl.legend(loc ='best')
    
    pl.subplot(3,1,2)
    pl.plot(errorSQ, color = 'r', label = 'Error with $f(x)=x^2$')
    pl.xlim([0., 1000])
    pl.yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
    pl.ylabel('$E[f(x)]$')    
    pl.title('Error plot' )
    pl.legend(loc ='best')
    
    pl.subplot(3,1,3)
    pl.semilogy(errorSQ, color = 'g', label = 'semilog plot of Error with $f(x)=x^2$')
    pl.plot(1/np.arange(1,100001)**0.5, label = '$1/N^{0.5}$', color = 'm')
    pl.yticks([0.000001, 0.0001, 0.01, 1.0])
    pl.title('loglog plot of error' )
    pl.xlabel('$N$')
    pl.legend(loc ='best')
    
    pl.show()

    print ''
    print ''
    print 'We see that the convergence looks exponential but with some'
    print 'computational/rounding errors for larger N.'
    print ''
    print ''
    
    print ''
    print ''
    print 'The End'
    print ''
    print ''

